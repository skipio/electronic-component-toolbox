## ELECTRONICS COMPONENT TOOLBOX

A simple resistor color-code calculator made for Android. It is currently published on the Android Play Store via [this link](https://play.google.com/store/apps/details?id=uk.ac.leeds.el17jkr.componenttoolbox).
