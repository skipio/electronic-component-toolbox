/*
AUTHOR: JAPHETH KIPLANGAT RONO
SID: 200942675
This fragment generates a view that displays all the decades of a certain resistance E-series in a
PopUp Window.
*/

package uk.ac.leeds.el17jkr.componenttoolbox;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.ArrayList;

public class tableFragment extends Fragment {
    // the fragment initialization parameters
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    // Buttons to trigger display of e-series table
    Button buttonE6;
    Button buttonE12;
    Button buttonE24;
    Button buttonE48;
    Button buttonE96;
    Button buttonE192;

    // placeholder for current view
    View view;

    // Dimensions of popUp window containing series values
    int popUpWindowHeight;
    int popUpWindowWidth;

    // 2-D array containing arrays containing E6, E12, E24, E48, E96 and E192 series
    int[][] seriesArray = {{100,150,220,330,470,680},
            {100,120,150,180,220,270,330,390,470,560,680,820},
            {100,110,120,130,150,160,180,200,220,240,270,300,330,360,390,430,470,510,560,620,680,
                    750,820,910},
            {100,105,110,115,121,127,133,140,147,154,162,169,178,187,196,205,215,226,237,249,261,
                    274,287,301,316,332,348,365,383,402,422,442,464,487,511,536,562,590,619,649,
                    681,715,750,787,825,866,909,953},
            {100,102,105,107,110,113,115,118,121,124,127,130,133,137,140,143,147,150,154,158,162,
                    165,169,174,178,182,187,191,196,200,205,210,215,221,226,232,237,243,249,255,
                    261,267,274,280,287,294,301,309,316,324,332,340,348,357,365,374,383,392,402,
                    412,422,432,442,453,464,475,487,499,511,523,536,549,562,576,590,604,619,634,
                    649,665,681,698,715,732,750,768,787,806,825,845,866,887,909,931,953,976},
            {100,101,102,104,105,106,107,109,110,111,113,114,115,117,118,120,121,123,124,126,127,
                    129,130,132,133,135,137,138,140,142,143,145,147,149,150,152,154,156,158,160,
                    162,164,165,167,169,172,174,176,178,180,182,184,187,189,191,193,196,198,200,
                    203,205,208,210,213,215,218,221,223,226,229,232,234,237,240,243,246,249,252,
                    255,258,261,264,267,271,274,277,280,284,287,291,294,298,301,305,309,312,316,
                    320,324,328,332,336,340,344,348,352,357,361,365,370,374,379,383,388,392,397,
                    402,407,412,417,422,427,432,437,442,448,453,459,464,470,475,481,487,493,499,
                    505,511,517,523,530,536,542,549,556,562,569,576,583,590,597,604,612,619,626,
                    634,642,649,657,665,673,681,690,698,706,715,723,732,741,750,759,768,777,787,
                    796,806,816,825,835,845,856,866,876,887,898,909,920,931,942,953,965,976,988}};

    public tableFragment() {
        // Required empty public constructor
    }

    // Default listener of fragment//
    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment tableFragment.
     */
    public static tableFragment newInstance(String param1, String param2) {
        tableFragment fragment = new tableFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_table, container, false);

        // PopUp Window Dimensions set
        popUpWindowHeight = (int)(getResources().getDimension(R.dimen.popup_element_height)*2 +
                getResources().getDimension(R.dimen.popup_scroll_height));
        popUpWindowWidth = (int)getResources().getDimension(R.dimen.popup_scroll_width);

        // Listener that makes buttons trigger the appearance of a populated popUp window.
        View.OnClickListener buttonListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Activity context = tableFragment.this.getActivity(); // this fragment

                // The view triggering the event is identified and cast as a Button since only
                // buttons have this listener.
                Button b = (Button)v;

                // String containing the PopUp Window Title is set to the name of the Button
                // triggering the event.
                String buttonName = b.getText().toString();
                String popUpTitle = buttonName + " Resistor Series";

                // The PopUp Window Layout is a linear layout with a title as a TextView, a
                // listView containing the series resistor decade values and a button for
                // dismissing the popup window.
                LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.popup);
                LayoutInflater layoutInflater = (LayoutInflater) context.
                        getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View layout = layoutInflater.inflate(R.layout.popup_layout, viewGroup);

                // The PopUp window is created and sized.
                final PopupWindow popUp = new PopupWindow(context);
                popUp.setContentView(layout);
                popUp.setWidth(popUpWindowWidth);
                popUp.setHeight(popUpWindowHeight);
                popUp.setFocusable(true);

                popUp.setBackgroundDrawable(new BitmapDrawable()); // Popup made transluscent

                // PopUp title is setup and it's text is set.
                TextView popUpViewTitle = (TextView) layout.findViewById(R.id.popupTitle);
                popUpViewTitle.setText(popUpTitle);

                // A ListView is created to be populated with resistor decade values.
                ListView seriesList = (ListView) layout.findViewById(R.id.series_listing);

                ArrayList<String> mySeriesList = new ArrayList<String>();   // to contain decade strings

                // Finds the correct index for the seriesArray from the button triggering the event.
                // For instance, E6-> 6 -> index of 0
                int index = (int) (Math.log(Integer.
                        parseInt(buttonName.substring(1))/6)/Math.log(2));

                // Populate the ArrayList with decade values
                for(int i: seriesArray[index]) {
                    mySeriesList.add(String.valueOf(i));
                }

                // Set the ListView adapter to contain the list of decade values
                ArrayAdapter<String> seriesAdapter = new ArrayAdapter<String>(context,
                        android.R.layout.simple_list_item_1, mySeriesList);
                seriesList.setAdapter(seriesAdapter);

                // Show popup at center of screen
                popUp.showAtLocation(layout, Gravity.CENTER, 0, 0);

                // Setup dismiss button to dismiss popup window
                Button dismiss_popUp = (Button) layout.findViewById(R.id.button_dismiss);
                dismiss_popUp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popUp.dismiss();
                    }
                });
            }
        };

        // Setup buttons and connect the click listener named buttonListener
        buttonE6 = view.findViewById(R.id.buttonE6);
        buttonE6.setOnClickListener(buttonListener);

        buttonE12 = view.findViewById(R.id.buttonE12);
        buttonE12.setOnClickListener(buttonListener);

        buttonE24 = view.findViewById(R.id.buttonE24);
        buttonE24.setOnClickListener(buttonListener);

        buttonE48 = view.findViewById(R.id.buttonE48);
        buttonE48.setOnClickListener(buttonListener);

        buttonE96 = view.findViewById(R.id.buttonE96);
        buttonE96.setOnClickListener(buttonListener);

        buttonE192 = view.findViewById(R.id.buttonE192);
        buttonE192.setOnClickListener(buttonListener);

        return view;
    }

    // Default Fragment UI event method
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
