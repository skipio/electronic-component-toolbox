/*
AUTHOR: JAPHETH KIPLANGAT RONO
SID: 200942675
This application is a component calculator with 4 different views. The views are toggled using the
Bottom Navigation Bar at the base of the display. The views are fragments.

The views are:
- 4-band resistor view
- 5-band resistor view
- tantalum capacitor view
- resistor series table view
*/

package uk.ac.leeds.el17jkr.componenttoolbox;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity {
    FrameLayout contentLayout;      // element containing different views

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            // Setting up Listener to swap between views using the fragment manager.
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();

            // Switching between different fragments based on the id of the icon pressed in the
            // Bottom Navigation Panel
            switch (item.getItemId()) {
                case R.id.navigation_4_band:    // 4-band resistor view
                    transaction.replace(R.id.content, new fourBandFragment()).commit();
                    return true;
                case R.id.navigation_5_band:    // 5-band resistor view
                    transaction.replace(R.id.content, new fiveBandFragment()).commit();
                    return true;
                case R.id.navigation_capacitor: // tantalum capacitor view
                    transaction.replace(R.id.content, new capacitorFragment()).commit();
                    return true;
                case R.id.navigation_table:     // resistor series table view
                    transaction.replace(R.id.content, new tableFragment()).commit();
                    return true;

            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setup the navigation object as a BottomNavigationView Object with a listener that will
        // swap views upon touch input.
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // Setting up the layout to populate the Activity Window
        contentLayout = findViewById(R.id.content);

        // Setting up the 4-band resistor view as the default view
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.content, new fourBandFragment()).commit();
    }

}
