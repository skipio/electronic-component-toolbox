/*
AUTHOR: JAPHETH KIPLANGAT RONO
SID: 200942675
This fragment generates a view that calculates the resistor value based on values of the 5 spinners
on the display. It can also search for the said resistor in select search engines.
*/

package uk.ac.leeds.el17jkr.componenttoolbox;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

public class fiveBandFragment extends Fragment {
    // the fragment initialization parameters
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private TextView resistorVal; // Text Box containing the calculated resistor value

    // Custom Spinners displaying the four bands of the 4-band resistor
    private ScrollView spinner1;
    private ScrollView spinner2;
    private ScrollView spinner3;
    private ScrollView spinner4;
    private ScrollView spinner5;

    private Spinner spinnerURL;     // Spinner containing possible search engines

    Button goButton;                // Toggle search of component

    String[] resistor_code_tolerance;   // Array of resistor tolerance values in 5th band

    int cellHeight; // height of cell within custom spinners

    String prefix;  // prefix of resistance value for kilo-, mega-, giga- ohm

    String valText; // resistance value without prefix

    // rText() calculates the value of the resistance based on the corresponding 4-band resistor
    // values.
    private String rText() {
        String finalText;   // Contains the final resistance value to be displayed in textBox

        // Calculation of values from custom spinner based on the location of the scrollView
        int val1 = ((int) (spinner1.getScrollY() + (cellHeight / 1.5) / cellHeight)) / cellHeight;
        int val2 = ((int) (spinner2.getScrollY() + (cellHeight / 1.5) / cellHeight)) / cellHeight;
        int val3 = ((int) (spinner3.getScrollY() + (cellHeight / 1.5) / cellHeight)) / cellHeight;
        int val4 = (((((int) (spinner4.getScrollY() + (cellHeight / 1.5) / cellHeight))
                / cellHeight) + 2) % 10) - 2;
        String val5 = resistor_code_tolerance[(int) (((int) (spinner5.getScrollY() +
                (cellHeight / 1.5) / cellHeight)) / cellHeight)];

        // Raw resistance value from custom spinners
        double val = ((val1 *100) + (val2 *10) + val3)*Math.pow(10, val4);

        // Resistance value is corrected such that it displays max 999 along with the needed suffix
        if (val >= 1000) {
            val /= 1000;
            if (val >= 1000) {
                val /= 1000;
                if (val >= 1000) {
                    val /= 1000;
                    prefix = "G";
                } else {
                    prefix = "M";
                }
            } else {
                prefix = "k";
            }
        } else {
            prefix = "";
        }

        // The value to be displayed is corrected to display 24.0 as 24 but 24.1 as 24.1
        if (val % 1 == 0) {
            valText = Integer.toString((int)(val));
        } else {
            valText = Double.toString(Double.parseDouble(String.
                    format("%4.4s", Double.toString(val))));
        }

        finalText = valText + " " + prefix + "Ω " + val5;   // The final display value is generated.

        return finalText;
    }

    // Default listener of fragment
    private OnFragmentInteractionListener mListener;

    public fiveBandFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment fiveBandFragment.
     */
    public static fiveBandFragment newInstance(String param1, String param2) {
        fiveBandFragment fragment = new fiveBandFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_five_band, container, false);

        // The custom spinner cell height is drawn from the resources folder of the project
        cellHeight = getResources().getDimensionPixelSize(R.dimen.cell_height);

        // Values for the resistor code, power and tolerance custom spinners are drawn from the
        // resources folder of the project
        String[] resistor_code = getResources().getStringArray(R.array.resistor_code);
        int[] resistor_code_colors = getResources().getIntArray(R.array.resistor_colors);
        int[] resistor_code_colors_text = getResources().getIntArray(R.array.resistor_colors_text);

        String[] resistor_code_power = getResources().getStringArray(R.array.resistor_power);
        int[] resistor_code_power_colors = getResources().
                getIntArray(R.array.resistor_power_colors);
        int[] resistor_code_power_colors_text = getResources().
                getIntArray(R.array.resistor_power_colors_text);

        resistor_code_tolerance = getResources().getStringArray(R.array.resistor_tolerance);
        int[] resistor_code_tolerance_colors = getResources().
                getIntArray(R.array.resistor_tolerance_colors);
        int[] resistor_code_tolerance_colors_text = getResources().
                getIntArray(R.array.resistor_tolerance_colors_text);

        // The ScrollViews present in the layout file of this fragment are called to be customized
        spinner1 = (ScrollView) view.findViewById(R.id.scroller1);
        spinner2 = (ScrollView) view.findViewById(R.id.scroller2);
        spinner3 = (ScrollView) view.findViewById(R.id.scroller3);
        spinner4 = (ScrollView) view.findViewById(R.id.scroller4);
        spinner5 = (ScrollView) view.findViewById(R.id.scroller5);

        // The ScrollViews are individually customized by adding internal Linear Layout with cells
        // containing TextViews each with a resistor code value, the corresponding background color
        // and text Color for correction (if the background color is similar to text Color)
        createPicker(spinner1, resistor_code, resistor_code_colors_text, resistor_code_colors);
        createPicker(spinner2, resistor_code, resistor_code_colors_text, resistor_code_colors);
        createPicker(spinner3, resistor_code, resistor_code_colors_text, resistor_code_colors);
        createPicker(spinner4, resistor_code_power, resistor_code_power_colors_text,
                resistor_code_power_colors);
        createPicker(spinner5, resistor_code_tolerance, resistor_code_tolerance_colors_text,
                resistor_code_tolerance_colors);

        // The TextView displaying the final resistance value
        resistorVal = (TextView) view.findViewById(R.id.textView3);

        // The resistance value is set to 0 initially.
        resistorVal.setText(rText());

        // The spinner containing the URL list is called from the layout and assigned an adapter
        // containing a string of electronic component search engines from the resources folder
        // of the project.
        spinnerURL = view.findViewById(R.id.spinnerURL);
        ArrayAdapter<CharSequence> urlAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.url_list, android.R.layout.simple_spinner_item);
        urlAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerURL.setAdapter(urlAdapter);

        // An array of strings of urls for the corresponding search engines
        final String[] urlList = getResources().getStringArray(R.array.url_query_list);

        // Button to trigger search of component
        goButton = view.findViewById(R.id.goButton);

        // Listener set to goButton to acquire url from spinnerURL and open Android web browser
        goButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = urlList[spinnerURL.getSelectedItemPosition()]+
                        valText+"+"+prefix+"ohm+resistor";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                getActivity().startActivity(i);
            }
        });

        return view;
    }

    // Default Fragment UI event method
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    // createPicker() customizes a ScrollView to include a Linear Layout containing
    // customized TextViews and listeners to detect scrolling and the end of scrolling.
    public void createPicker(final ScrollView scV, String[] viewText, int[] viewTextColor,
                             int[] viewBGndColor) {
        scV.setVerticalScrollBarEnabled(false);     // No scrollbar in spinner
        // Preventing blue light upon scrolling to edge
        scV.setOverScrollMode(ScrollView.OVER_SCROLL_NEVER);

        // The only child of the spinner is this linear layout which will contain all the needed
        // TextViews with corresponding values
        LinearLayout linL = new LinearLayout(this.getContext());
        linL.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        linL.setOrientation(LinearLayout.VERTICAL);     // Linear Layout is vertical

        int size = viewText.length; // Determines how many cells will be in spinner

        scV.addView(linL);  // Add the child layout to the custom spinner

        // An empty TextView is added such that the first value, for example 0 in the first band,
        // can still be at the center of the spinner.
        TextView emptyTv = new TextView(getContext());
        // The TextView is made distinct from other cells in the spinner but with same dimensions
        emptyTv.setBackgroundColor(Color.GRAY);
        emptyTv.setHeight(getResources().getDimensionPixelSize(R.dimen.cell_height));
        emptyTv.setWidth(getResources().getDimensionPixelSize(R.dimen.picker_width));
        linL.addView(emptyTv);  // Add empty TextView to linear layout

        // From the string of values to be displayed in the spinner, corresponding textViews are
        // created with the respective background color and text color.
        for (int i = 0; i < size; i++) {
            TextView tv = new TextView(getContext());
            tv.setText(viewText[i]);
            tv.setGravity(Gravity.CENTER);  // Text is centered in the TextView
            tv.setBackgroundColor(viewBGndColor[i]);
            tv.setTextColor(viewTextColor[i]);
            tv.setHeight(getResources().getDimensionPixelSize(R.dimen.cell_height));
            tv.setWidth(getResources().getDimensionPixelSize(R.dimen.picker_width));
            linL.addView(tv);   // Add customized TextView to linear layout
        }

        // An empty TextView is added such that the last value, for example 9 in the first band,
        // can still be at the center of the spinner.
        TextView emptyTv2 = new TextView(getContext());
        emptyTv2.setBackgroundColor(Color.GRAY);
        emptyTv2.setHeight(getResources().getDimensionPixelSize(R.dimen.cell_height));
        emptyTv2.setWidth(getResources().getDimensionPixelSize(R.dimen.picker_width));
        linL.addView(emptyTv2);

        // This listener updates the textView containing the final resistance during scrolling.
        scV.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                resistorVal.setText(rText());
            }
        });

        // This listener detects the end of a scrolling event and snaps the scroll view to the
        // closest cell
        scV.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP || event.
                        getAction() == MotionEvent.ACTION_CANCEL) {
                    int scrollY = scV.getScrollY();     // Scrolling height of spinner
                    // New position that the spinner will snap to
                    int positionY = ((int)(scrollY+cellHeight/1.5)/cellHeight)*cellHeight;
                    scV.smoothScrollTo(0, positionY);   // Smoothly correct spinner
                    resistorVal.setText(rText());   // Update resistance value displayed
                    return true;
                } else {
                    return false;
                }

            }
        });
    }
}
